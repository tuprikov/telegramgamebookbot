"""
Console interface for URQL parser
"""
import argparse
import logging

from pathlib import Path
from utils.urql_parser import BookMode, URQLParser

def get_choice(message: str, choices: list, inventory_mode = False):
    """
    Prints provided message and available choices and returns user's choice
    :param str message: Message to print
    :param list choices: Available choices
    :param bool inventory_mode: False for general interaction mode, true for inventory mode
    """
    print(message)
    result = None
    while result is None:
        print('Выберите вариант от 1 до {0}:'.format(len(choices)))
        for num, choice in enumerate(choices, 1):
            print(num, ':', choice['desc'])
        #print(0, ':', 'Выйти из игры.')
        inp = input()
        if inp == 'i' and not inventory_mode:
            result = '__open_inventory__'
        else:
            try:
                inp = int(inp)
            except ValueError:
                print('Необходимо ввести число, соответствующее номеру варианта!')
                continue
            if inp == 0:
                return None
            elif inp < 0 or inp > len(choices):
                print('Число должно быть от 1 до {0}!'.format(len(choices)))
            else:
                result = choices[inp - 1]['label']
    print('')
    return result

def get_input(message : str):
    """
    Prints provided message and returns user's plain text input
    """
    print(message)
    inp = input()
    print('')
    return inp

def main():
    """
    Main function
    """
    parser = argparse.ArgumentParser(description='Console UI for URQL parser.')
    parser.add_argument('--file', metavar='filename', type=str, help='filename of the gamebook')
    parser.add_argument('--debug', action='store_true', help='debug mode')
    args = parser.parse_args()

    if args.debug:
        debug_level = logging.DEBUG
    else:
        debug_level = logging.INFO
    if args.file is None:
        books_path = Path(__file__).resolve().parent.parent / 'books'
        books_list = [ {'label': file, 'desc': file.name} for file in books_path.iterdir() if file.is_file() ]
    while True:
        if args.file is None:
            book_name = get_choice('Добро пожаловать в консольный интерфейс парсера URQL! Какую книгу хотите сыграть?',
                                   books_list)
            if book_name is None:
                print('Консольный интерфейс парсера URQL завершает работу.')
                return
        else:
            book_name = args.file
        urql = URQLParser(debug_level)
        try:
            (lines, labels) = urql.open_book(book_name)
        except Exception as book_exception:
            print(book_exception)
            continue
        variables = {}
        line_num = 0
        skip_interact = False
        book_mode = BookMode.MODE_DEFAULT
        choice = None
        input_var = None
        input_str = ''
        while True:
            if not skip_interact:
                result = urql.interact(lines, labels, variables, line_num, book_mode, choice, input_var, input_str)
            else:
                skip_interact = False
            if result['mode'] == BookMode.MODE_CHOICE:
                book_mode = BookMode.MODE_CHOICE
                choice = get_choice(result['text'], result['choices'])
                if choice is None:
                    print('Игра прервана.')
                    return
                elif choice == '__open_inventory__':
                    choice_inv = None
                    skip_interact = True
                    while True:
                        result_inv = urql.open_inventory(variables)
                        result_inv['choices'].append({'label': '__close_inventory__', 'desc': 'Закрыть инвентарь'})
                        choice_inv = get_choice(result_inv['text'], result_inv['choices'], True)
                        if choice_inv is None:
                            print('Игра прервана.')
                            return
                        elif choice_inv == '__close_inventory__':
                            break
                        elif choice_inv.startswith('__item_'):
                            result_item = urql.open_item_actions(labels, choice_inv[7:])
                            result_item['choices'].append({'label': '__close_item__', 'desc': 'Вернуться в инвентарь'})
                            choice_item = get_choice(result_item['text'], result_item['choices'], True)
                            if choice_item is None:
                                print('Игра прервана.')
                                return
                            elif choice_item == '__close_item__':
                                continue
                            elif choice_item.lower().startswith('use_'):
                                result_use_item = urql.interact(lines, labels, variables, line_num,
                                                                mode=BookMode.MODE_PROC, label=choice_item,
                                                                return_message=True)
                                if result_use_item['mode'] == BookMode.MODE_CHOICE:
                                    result['text'] = result_use_item['text']
                                    result['choices'] = result_use_item['choices']
                                    book_mode = BookMode.MODE_CHOICE
                                    choice = None
                                else:
                                    print(result_use_item['text'])
                                break
                        else:
                            print(urql.interact(lines, labels, variables, line_num, mode=BookMode.MODE_PROC,
                                                label=choice_inv, return_message=True)['text'])
            elif result['mode'] == BookMode.MODE_INPUT:
                book_mode = BookMode.MODE_INPUT
                input_var = result['input_var']
                input_str = get_input(result['text'])
                line_num = result['line_num']
            elif result['mode'] == BookMode.MODE_ERROR:
                print(result['text'])
                break

if __name__ == "__main__":
    main()

import json
import logging

from webhook.models import Book, InteractionType, User, UserBookInteraction, UserBookVariable, UserMode
from utils.urql_parser import BookMode, URQLParser

class GamebookReply:
    chat_id = 0
    text = ''
    choices = None

    def __init__(self, chat_id : int, text : str, choices = None):
        self.chat_id = chat_id
        self.text = text
        self.choices = choices

    def __str__(self):
        return '{} {}'.format(self.text, self.choices)
    def __repr__(self):
        return self.__str__()

class GamebookMessageProcessor(URQLParser):
    #lines = None # processed (from a file) and cached lines of the book
    chat_id = 0
    message_queue = None # list of messages to send back to the user
    #bookshelf = None # list of active books (cache)

    def __init__(self, debug_level=logging.DEBUG):
        super().__init__(debug_level=debug_level)
    # def __init__(self, debug_level=logging.DEBUG):
    #     #logging.basicConfig(level=logging.DEBUG)
    #     super().__init__(debug_level=debug_level)
    #     self.message_queue = []
    #     #self.bookshelf = {}
    #     logging.debug('GMP initialized.')

    def _append_final_message_(self, msg_src, mode = UserMode.BOOK_OPEN, add_inventory_button = True):
        #logging.debug('Append final mesasge: %s', msg_src)
        buttons_list = None
        if msg_src['mode'] == BookMode.MODE_DEFAULT:
            buttons_list = [ [choice['desc'], ] for choice in msg_src['choices'] ]
        elif msg_src['mode'] == BookMode.MODE_CHOICE:
            buttons_list = [ [choice['desc'], ] for choice in msg_src['choices'] ]
            if mode == UserMode.BOOK_OPEN:
                if add_inventory_button:
                    buttons_list.append([u'\U0001F392 Инвентарь', u'\u274C Прервать игру'])
                else:
                    buttons_list.append([u'\u274C Прервать игру', ])
            elif mode == UserMode.INVENTORY:
                buttons_list.append([u'\u274C Закрыть инвентарь', ])
            elif mode == UserMode.INVENTORY_ITEM:
                buttons_list.append([u'\u2B05 Назад', ])
        reply = GamebookReply(self.chat_id, msg_src['text'], buttons_list)
        self.message_queue.append(reply)

    def _append_abort_message_(self):
        reply = GamebookReply(self.chat_id, 'Прервать игру? Весь прогресс будет утерян.', [['Прервать', ], ['Отмена', ]])
        self.message_queue.append(reply)

    def _get_variables_(self, user):
        return { record.name: record.value if record.float_value is None else record.float_value for record in UserBookVariable.objects.filter(user_id=user.id, book_id=user.current_book) }

    def _get_last_interaction_(self, user, interaction_type):
        try:
            interaction = UserBookInteraction.objects.get(user_id=user.id, book_id=user.current_book, type=interaction_type)#.__dict__
            return { 'mode': interaction.book_mode, 'text': interaction.text, 'choices': json.loads(interaction.choices) }
        except UserBookInteraction.DoesNotExist:
            return None

    def _save_user_progress_(self, user, variables, interactions):
        # saving general information
        user.save()
        logging.debug('User %s saved.', str(user))
        # saving variables
        UserBookVariable.objects.filter(user_id=user.id, book_id=user.current_book).delete()
        if not variables is None and len(variables) > 0:
            variables_list = []
            for (i_name, i_value) in variables.items():
                if isinstance(i_value, float):
                    variables_list.append(UserBookVariable(user_id=user, book_id=user.current_book, name=i_name, float_value=i_value))
                else:
                    variables_list.append(UserBookVariable(user_id=user, book_id=user.current_book, name=i_name, value=i_value))
            UserBookVariable.objects.bulk_create(variables_list)
            #UserBookVariable.objects.bulk_create([ UserBookVariable(user_id=user, book_id=user.current_book, name=i_name, value=i_value) for (i_name, i_value) in variables.items() ])
        # saving interactions
        UserBookInteraction.objects.filter(user_id=user.id, book_id=user.current_book).delete()
        #logging.debug('Save user progress interactions: %s', interactions)
        if not interactions is None and len(interactions) > 0:
            UserBookInteraction.objects.bulk_create([ UserBookInteraction(user_id=user, book_id=user.current_book, type=i_type, book_mode=i_interaction['mode'], text=i_interaction['text'], choices=json.dumps(i_interaction.get('choices'))) for (i_type, i_interaction) in interactions.items() ])
        return

    def _get_book_(self, bookshelf: dict, book_id = None, book_name = None):
        if not book_id is None:
            book = Book.objects.get(id = book_id)
        else:
            if not book_name is None:
                book = bookshelf.get(book_name)
                if book is None:
                    book = Book.objects.get(name = book_name)
                    logging.debug('Book %s is not found on the bookshelf, retrieved from the DB.', book_name)
                else:
                    logging.debug('Book %s is taken from the bookshelf.', book_name)
            bookshelf.update({book_name: book})
        return book

    def process(self, bookshelf: dict, msg_user: User, msg_text: str):
        #logging.debug('GMP.process started with bookshelf %s for user %s, text %s.', bookshelf, msg_user, msg_text)
        #print('GMP.process started with bookshelf {} for user {}, text {}.'.format(bookshelf, msg_user, msg_text))
        self.message_queue = []
        book = None
        variables = self._get_variables_(msg_user)
        last_interaction = self._get_last_interaction_(msg_user, InteractionType.GENERAL)
        last_inv_interaction = self._get_last_interaction_(msg_user, InteractionType.INVENTORY)
        last_inv_item_interaction = self._get_last_interaction_(msg_user, InteractionType.INVENTORY_ITEM)

        # Message is processed according to user status
        while True:
            if msg_user.mode == UserMode.START:
                # send book choosing message
                logging.debug('User %s mode is START.', msg_user)
                msg_user.current_book = None
                variables = {}
                last_interaction = None
                last_inv_interaction = None
                last_inv_item_interaction = None
                msg_user.mode = UserMode.BOOK_CHOICE
                first_interaction = {'mode': BookMode.MODE_DEFAULT, 'text': 'В какую книгу желаете поиграть?', 'choices': [ {'desc': book_entry.name} for book_entry in list(Book.objects.all()) ]}
                #reply = GamebookReply(self.chat_id, 'В какую книгу желаете поиграть?', [ [book_entry.name, ] for book_entry in list(Book.objects.all()) ])
                self._append_final_message_(first_interaction)
                #self.message_queue.append(reply)
                break
            else:
                if book is None and not msg_user.current_book is None:
                    logging.debug('Assigning user %s current book %s to book variable.', msg_user, msg_user.current_book)
                    book = msg_user.current_book
                    if not isinstance(book, Book) or book.lines is None or book.lines == []:
                        try:
                            (book.lines, book.labels) = self.open_book(book.full_path())
                        except Exception as book_exception:
                            reply = GamebookReply(self.chat_id, str(book_exception))
                            self.message_queue.append(reply)
                            msg_user.mode = UserMode.START
                            continue

            if msg_user.mode == UserMode.BOOK_CHOICE:
                logging.debug('User %s mode is CHOICE.', msg_user)
                if book is None or book.name != msg_text:
                    try:
                        book = self._get_book_(bookshelf, book_name=msg_text)
                    except Book.DoesNotExist:
                        reply = GamebookReply(self.chat_id, 'Такая книга отсутствует!')
                        self.message_queue.append(reply)
                        msg_user.mode = UserMode.START
                        continue
                    except Book.MultipleObjectsReturned:
                        reply = GamebookReply(self.chat_id, 'Найдено несколько книг с таким названием!')
                        self.message_queue.append(reply)
                        msg_user.mode = UserMode.START
                        continue
                else:
                    logging.debug('Book: %s, lines: %s', book, len(book.lines))
                if book.lines is None or book.lines == []:
                    logging.debug('Book.isnt ready, reopening.')
                    try:
                        (book.lines, book.labels) = self.open_book(book.full_path())
                    except Exception as book_exception:
                        # failed to open book, return error message and move to start
                        reply = GamebookReply(self.chat_id, str(book_exception))
                        msg_user.mode = UserMode.START
                        continue
                msg_user.mode = UserMode.BOOK_OPEN
                msg_user.current_book = book
                msg_user.current_line = 0
                continue

            if msg_user.mode == UserMode.BOOK_OPEN:
                logging.debug('User %s mode is OPEN.', msg_user)
                repeat_previous_message = False
                params = { 'lines': book.lines, 'labels': book.labels, 'variables': variables, 'line_num': msg_user.current_line }
                if not last_interaction is None: # first interaction
                    params.update({'mode': last_interaction['mode']})
                    if last_interaction['mode'] == BookMode.MODE_CHOICE: # last interaction result was 'choice' - expecting user choice
                        if msg_text == u'\U0001F392 Инвентарь': # inventory button pressed
                            msg_user.mode = UserMode.INVENTORY
                            continue
                        elif msg_text == u'\u274C Прервать игру': # abort game button pressed
                            self._append_abort_message_()
                            msg_user.mode = UserMode.BOOK_ABORT
                            break
                        else:
                            # find out, which choice was made by user
                            query = [ item for item in last_interaction['choices'] if item['desc'] == msg_text ]
                            if len(query) > 0:
                                params.update({'label': query[0]['label']})
                            else:
                                reply = GamebookReply(self.chat_id, 'Необходимо выбрать вариант из предложенных!')
                                self.message_queue.append(reply)
                                repeat_previous_message = True
                    elif last_interaction['mode'] == BookMode.MODE_INPUT:
                        params.update({'input_var': msg_user.input_variable})
                        params.update({'input_str': msg_text})
                    else:
                        params.update({'label': msg_text})

                if not repeat_previous_message: # everything went well - making next interaction
                    result = self.interact(**params)
                    logging.debug('Book interaction result: %s', result)
                    if result['mode'] == BookMode.MODE_ERROR:
                        logging.error('Chat ID %s, %s', self.chat_id, result['text'])
                        reply = GamebookReply(self.chat_id, result['text'])
                        self.message_queue.append(reply)
                        repeat_previous_message = True
                    else:
                        if result['mode'] == BookMode.MODE_INPUT:
                            msg_user.input_variable = result['input_var']
                            msg_user.current_line = result['line_num']
                        last_interaction = result

                if last_interaction is None: # book returned an error on the first interaction
                    logging.debug('Last interaction is None, restarting.')
                    msg_user.mode = UserMode.START
                    continue
                else: # finally sending message to the user
                    self._append_final_message_(last_interaction, UserMode.BOOK_OPEN, book.supports_inventory)
                    break

            if msg_user.mode == UserMode.BOOK_ABORT:
                if msg_text == 'Прервать':
                    # game aborted -- get back to the start
                    reply = GamebookReply(self.chat_id, 'Игра прервана.')
                    self.message_queue.append(reply)
                    msg_user.mode = UserMode.START
                    continue
                elif msg_text == 'Отмена':
                    # user changed his mind -- get back to the game
                    msg_user.mode = UserMode.BOOK_OPEN
                    self._append_final_message_(last_interaction)
                    break
                else:
                    # user replied something unexpected -- repeat the abort message
                    self._append_abort_message_()
                    break

            if msg_user.mode == UserMode.INVENTORY:
                if msg_text == u'\U0001F392 Инвентарь':
                #if msg_user['last_inv_interaction'] is None:
                    # open inventory root
                    result_inv = self.open_inventory(variables)
                    if result_inv['mode'] == BookMode.MODE_ERROR:
                        logging.error('Chat ID %s, %s', self.chat_id, result_inv['text'])
                        reply = GamebookReply(self.chat_id, result_inv['text'])
                        self.message_queue.append(reply)
                        msg_text = u'\u274C Закрыть инвентарь' # TODO: use separate variable
                        continue
                    else:
                        last_inv_interaction = result_inv
                        self._append_final_message_(result_inv, UserMode.INVENTORY)
                    break
                elif msg_text == u'\u274C Закрыть инвентарь':
                    msg_user.mode = UserMode.BOOK_OPEN
                    self._append_final_message_(last_interaction)
                    last_inv_interaction = None
                    break
                else: # inventory item selected
                    # find out, which item was selected
                    query = [ item for item in last_inv_interaction['choices'] if item['desc'] == msg_text ] # TODO: code repeats itself
                    if len(query) > 0:
                        item_name = query[0]['label'][7:]
                        result_item = self.open_item_actions(book.labels, item_name)
                        if result_item['mode'] == BookMode.MODE_ERROR:
                            logging.error('Chat ID %s, %s', self.chat_id, result_inv.text)
                            reply = GamebookReply(self.chat_id, result_inv.text)
                            self.message_queue.append(reply)
                            msg_text = u'\U0001F392 Инвентарь' # TODO: use separate variable
                            continue
                        else: # item is found
                            msg_user.mode = UserMode.INVENTORY_ITEM
                            self._append_final_message_(result_item, UserMode.INVENTORY_ITEM)
                            last_inv_item_interaction = result_item
                        break
                    else:
                        reply = GamebookReply(self.chat_id, 'Такого предмета нет в инвентаре!')
                        self.message_queue.append(reply)
                        msg_text = u'\U0001F392 Инвентарь' # TODO: use separate variable
                        continue

            if msg_user.mode == UserMode.INVENTORY_ITEM:
                if msg_text == u'\u2B05 Назад':
                    self._append_final_message_(last_inv_interaction, UserMode.INVENTORY)
                    last_inv_item_interaction = None
                    msg_user.mode = UserMode.INVENTORY
                    break
                else:
                    query = [ item for item in last_inv_item_interaction['choices'] if item['desc'] == msg_text ] # TODO: code repeats itself
                    if len(query) > 0:
                        result = self.interact(lines=book.lines, labels=book.labels, variables=variables, line_num=msg_user.current_line, mode=BookMode.MODE_PROC, label=query[0]['label'], return_message=True)
                        logging.debug('Inventory item interaction result: %s', result['text'])
                        if result['mode'] == BookMode.MODE_CHOICE:
                            last_interaction = result
                            msg_user.mode = UserMode.BOOK_OPEN
                            self._append_final_message_(last_interaction)
                            last_inv_interaction = None
                            last_inv_item_interaction = None
                            break
                        else:
                            reply = GamebookReply(self.chat_id, result['text'])
                            self.message_queue.append(reply)
                            #logging.debug('Current message queue: %s', self.message_queue)
                            msg_text = u'\U0001F392 Инвентарь' # TODO: use separate variable
                            msg_user.mode = UserMode.INVENTORY
                            continue
                    else:
                        reply = GamebookReply(self.chat_id, 'Такое действие недоступно!')
                        self.message_queue.append(reply)
                        msg_text = u'\u2B05 Назад' # TODO: use separate variable
                        #msg_user['mode'] = 'inventory'
                        continue

        # we're out of the main loop -- it means the message queue is ready to send
        # saving user data
        interactions = {}
        if not last_interaction is None:
            interactions.update({InteractionType.GENERAL: last_interaction})
        if not last_inv_interaction is None:
            interactions.update({InteractionType.INVENTORY: last_inv_interaction})
        if not last_inv_item_interaction is None:
            interactions.update({InteractionType.INVENTORY_ITEM: last_inv_item_interaction})
        self._save_user_progress_(msg_user, variables, interactions)
        #logging.debug('GMP final message queue:\n%s', self.message_queue)
        return self.message_queue

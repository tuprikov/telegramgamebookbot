"""
Gamebook bot initial launch module
"""
import argparse
import os
import sys

import django
from django.core import management

from services.telegram_bot_service import TelegramBotService


def _set_webhook(url: str):
    """
    Assign webhook to specified URL
    """
    bot_service = TelegramBotService()
    result = bot_service.set_webhook(url) # 'https://gamebooks.veres72.ru/webhook'
    print('Webhook setting result: ', result.json())

def _get_webhook():
    """
    Get current webhook info
    """
    bot_service = TelegramBotService()
    result = bot_service.get_webhook_info()
    print('Current webhook info: ', result.json())

def main():
    """
    Main function
    """
    parser = argparse.ArgumentParser(description='Gamebook bot initial launch module.')
    parser.add_argument('--set-webhook', metavar='url', type=str, help='Set new webhook URL')
    parser.add_argument('--get-webhook', action='store_true', help='Get current webhook status')
    args = parser.parse_args()

    if not args.set_webhook is None:
        _set_webhook(args.set_webhook)
    elif args.get_webhook:
        _get_webhook()
    else:
        sys.path.append('./django_site')
        os.environ['DJANGO_SETTINGS_MODULE'] = 'gb_site.settings'
        django.setup()
        try:
            management.call_command('createsuperuser', interactive=False)
        except Exception:
            print('Adding Django superuser failed.')
        try:
            management.call_command('makemigrations', no_input=True)
            management.call_command('migrate', no_input=True)
        except Exception:
            print('Django migrations failed.')
        management.call_command('runserver', '--nothreading', '0.0.0.0:5000')

if __name__ == '__main__':
    main()

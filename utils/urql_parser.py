"""
Parser of URQL language.
https://ru.wikipedia.org/wiki/URQ
"""


import logging
import random
import re

from enum import IntEnum


class BookMode(IntEnum):
    """
    Book modes.
    """
    # Error encountered while reading.
    MODE_ERROR = -1
    # Starting mode.
    MODE_DEFAULT = 0
    # The book awaits for reader's choice.
    MODE_CHOICE = 1
    # The book awaits for reader's input in free form.
    MODE_INPUT = 2
    # The book is running a subroutine.
    MODE_PROC = 3
    # The book did a text output.
    MODE_TEXT = 3


class URQLParser():
    """
    Main class of a parser.
    """
    cur_message = None
    cur_buttons = None


    def __init__(self, debug_level=logging.DEBUG):
        logging.basicConfig(level=debug_level)
        self._wipe_current_message_and_buttons()

    def open_book(self, bookname: str) -> tuple:
        """
        Opens a book file for reading.
        param bookname: Path to a book file.
        return: Tuple (
            List of lines (text contents of the book),
            Dictionary of labels {label name: line number}
            )
        """
        exception_text = ''

        # reading the entire book from file
        with open(bookname, encoding='cp1251') as file_handle:
            book = file_handle.read()
            logging.debug('The book has been opened successfully.')
        # except OSError as file_not_found:
        #     exception_text = 'File not found: {}!'.format(bookname)
        #     logging.exception(exception_text)
        #     raise Exception(exception_text) from file_not_found

            book = self._remove_large_comments(book)

        lines = str.splitlines(book)

        # filling the label dictionary
        labels = {}
        for line_num, line in enumerate(lines):
            if line.startswith(':'):
                if len(line):
                    label = line[1:].rstrip()
                    test_line = labels.get(label)
                    if test_line:
                        ex_text = 'Line {}: Label "{}" was used earlier in line {}!'
                        exception_text += ex_text.format(line_num+1, label, test_line)
                    else:
                        labels[label] = line_num
                else:
                    exception_text += 'Line {}: Empty label!'.format(line_num+1)

        if exception_text:
            logging.exception(exception_text)
            raise Exception(exception_text)

        return (lines, labels)

    def _wipe_current_message_and_buttons(self):
        """
        Wipes current message and buttons.
        """
        self.cur_message = ''
        self.cur_buttons = []

    def _remove_large_comments(self, text: str) -> str:
        """
        Removes substrings enclosed into /* */.
        param text: Text string.
        """
        return re.sub(r'\/\*[\s\S]*?\*\/', '', text)
        # TODO: Legacy implementation:
        # cur = None
        # while cur != -1:
        #     if cur is None:
        #         cur = book.find('/*')
        #     else:
        #         cur = book.find('/*', cur)
        #     if cur > -1:
        #         cur_end = book.find('*/', cur)
        #         if cur_end > cur:
        #             book = book[0:cur] + book[cur_end+2:len(book)]
        #         else:
        #             exception_text += 'Position {}: Commentary has the beginning, but has no end.'.format(cur)

    def _check_condition(self, variables: dict, condition: str) -> bool:
        """
        Evaluates presumabely boolean string condition.
        param variables: Dictionary of current variables and their values.
        param condition: String condition.
        """
        # condition parsing
        value = re.sub(_REGEX_EQUAL, '==', condition)  # replace '=' with '=='
        value = re.sub('^inv_', 'inv_', value, flags = re.IGNORECASE)  # bring 'inv_' to lowercase
        # TODO: implement '===='
        # replace variables with their values and try to calculate
        try:
            value = re.sub(
                _REGEX_VARIABLES,
                lambda l : '{0}{1}{2}'.format(
                    l.group(1) or '',
                    self._get_variable_value(
                        variables,
                        l.group(2),
                        True
                        ),
                    l.group(3)
                    ),
                value
                )
            logging.debug('Trying to evaluate condition: %s', value)
            value = eval(value)
        except Exception as book_exception:
            exception_text = 'Failed to evaluate condition {0}!'.format(condition)
            logging.exception(exception_text)
            raise Exception(exception_text) from book_exception

        logging.debug('Condition %s calculation result = %s', condition, value)
        return value

    def _assign_variable(self, variables: dict, var: str, value: str, is_string_init: bool = False):
        """
        Assings a value to a specified variable.
        param variables: Dictionary of current variables and their values.
        param var: Variable name for assigning.
        param value: A value to assign.
        return: TODO: ?
        """
        var = re.sub('^inv_', 'inv_', var, flags = re.IGNORECASE)  # bring 'inv_' to lowercase
        # expression calculation
        old_value = value
        if not isinstance(value, str):
            logging.debug('Variable %s, value %s has a non-string type %s.', var, value, type(value))
        if not is_string_init:
            # replace 'rnd' with random values
            value = re.sub(_REGEX_RND, lambda l : self._get_rnd_value(l.group(1)), value)
            #value = re.sub(r'(?:\b)(rnd\d*)(?:\b)', lambda l : self._get_rnd_value(l.group(1)), '{0:g}'.format(value))
            # replace variables with their values
            value = re.sub(_REGEX_VARIABLES_ONLY, lambda l : self._get_variable_value(variables, l.group(1)), value)
            logging.debug('Trying to calculate expression: %s', value)
            # finally trying to calculate
            try:
                float_value = float(eval(value))
                #variables.update({var: '{0:g}'.format(float_value)}) # all values are stored as strings. '{0:g}'.format removes trailing zeros
                variables.update({var: float_value}) # no, had to change is. store floats as floats, strings as strings.
            except Exception:
                return (-1, 'Cannot process "{0}" expression. Initial expression: "{1}"'.format(value, old_value))
            if var.startswith('inv_') and float_value <= 0:
                del variables[var]  # inventory items with the value 0 or less should be deleted
            logging.debug('Expression %s calculation result = %s', old_value, variables[var])
        else:
            variables.update({var: str(value)})
            logging.debug('String variable %s was set to "%s".', var, value)

        return (variables.get(var, 0), )

    def _get_variable_value(self, variables: dict, var: str, string_with_quotes: bool = False) -> str:
        """
        Gets the value of a specified variable.
        param variables: Dictionary of current variables and their values.
        param var: Variable name for assigning.
        param string_with_quotes: If the value needs to be returned as a quoted string.
        return: Variable value as a string.
        """
        value = variables.get(var, float(0))
        if isinstance(value, float):
            return '{0:g}'.format(value)
        elif string_with_quotes:
            return '"' + str(value) + '"'

        return str(value)

    def _get_rnd_value(self, rnd_expr: str) -> str:
        """
        Generates a random value.
        param rnd_expr: Random expression:
            'rnd': Generates a random float value in the range [0.0, 1.0).
            'rndN': Generates a random integer value in the range [1, N].
        return: Random value as a string.
        """
        random.seed(random.random())
        if rnd_expr == 'rnd':
            rnd = str(random.random())
        else:
            arg = int(rnd_expr[3:])
            rnd = str(random.randint(1, arg))
        logging.debug('Random value: %s', rnd)

        return rnd

    def _variables_purge(self, variables: dict, inventory: bool):
        """
        Deletes all variables of specified kind from the dictionary.
        param variables: Dictionary of current variables and their values.
        param inventory: If True, delete inventory variables, otherwise delete simple variables.
        """
        for var in list(variables):
            if var.startswith('inv_') == inventory:
                del variables[var]

    def _inventory_add(self, variables: dict, var: str, value: float):
        """
        Adds specified amount to the specified inventory item.
        param variables: Dictionary of current variables and their values.
        param var: Inventory item name for adding.
        param value: A value to add.
        """
        var_name = 'inv_' + var
        inv_var = variables.get(var_name, 0)
        float_value = float(inv_var)
        float_value += value
        if float_value <= 0:
            if var_name in variables:
                del variables[var_name]
        else:
            variables[var_name] = float_value

    def _set_common_label_name(self, variables: dict, label: str) -> str:
        """
        Assigns common location label name.
        TODO: Check if it corresponds with URQL documentation.
        """
        if label.lower() == 'common':
            cur_cmn = self._get_variable_value(variables, 'common')
            if float(cur_cmn):
                return 'common_' + cur_cmn

        return label

    def _make_string_replacements(self, variables: dict, string: str) -> str:
        """
        Replaces #-values in the provided string.
        param variables: Dictionary of current variables and their values.
        string: String to process.
        return: String after replacements.
        """
        # insert ASCII codes
        new_text = re.sub(r'##(\d+?)\$', lambda l : chr(int(l.group(1))), string)
        # replace #$ with spaces
        new_text = re.sub(r'#\$', ' ', new_text)
        # replace #/$ with newline symbols
        new_text = re.sub(r'#/\$', '\n', new_text)
        # insert string variable values
        new_text = re.sub(r'#%?(.+?)\$', lambda l : self._get_variable_value(variables, l.group(1)), new_text)
        # insert number variable values
        new_text = re.sub(r'#?(.+?)\$', lambda l : self._get_variable_value(variables, l.group(1)), new_text)

        return new_text

    def interact(self, lines: list, labels: dict, variables: dict, line_num=0, mode=BookMode.MODE_DEFAULT, label=None, input_var=None, input_str='', return_message=False):
        if mode == BookMode.MODE_PROC:
            #label = args[1]
            #return_message = args[2]
            is_proc = True
            #line_num = self.labels[label]
            line_num = labels[label]
            if line_num is None:
                return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Команда "proc" указывает на несуществующую метку {}!'.format(line_num + 1, label) }
            else:
                logging.debug('Command "proc" moved to line %s.', line_num)
                #logging.debug('Line %s: Command "proc" moved to line %s.', prev_line_num, line_num)
        else:
            is_proc = False
            # setting current and previous locations (URQL system variables)
            #if not label is None and not label.lower().startswith('use_'):
            if not label is None and mode != BookMode.MODE_INPUT:
                # TODO: сравнить локации
                self._assign_variable(variables, 'previous_loc', self._get_variable_value(variables, 'current_loc'), True)
                self._assign_variable(variables, 'current_loc', label, True)

            self._wipe_current_message_and_buttons()
            logging.debug('Wiped current message and buttons.')

            if mode == BookMode.MODE_DEFAULT:
                pass

            elif mode == BookMode.MODE_CHOICE:
                #label = args[1]
                #self.cur_message = ''
                #self.cur_buttons = []
                label = self._set_common_label_name(variables, label)
                #line_num = self.labels[label]
                line_num = labels[label]
                if line_num is None:
                    return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Ссылка указывает на несуществующую метку {}!'.format(line_num + 1, label) }

            elif mode == BookMode.MODE_INPUT:
                self._assign_variable(variables, input_var, input_str, True)
                logging.debug('Line %s: Variable %s was set to %s by user input.', line_num, input_var, input_str)

        switch_mode_to_choice = False
        proc_buttons = []
        #if return_message:
        proc_message = ''

        while line_num < len(lines):
            # cut off small comment and whitespaces
            line = re.sub(';.*', '', lines[line_num]).strip()
            #self._make_string_replacements(variables, line)

            # skip empty line
            if len(line) == 0:
                line_num += 1
                continue

            # skip labels
            if re.match(':', line):
                line_num += 1
                continue

            # 'if' parsing
            if re.match('if ', line, re.IGNORECASE):
                # split if|then(|else)
                if re.search(' else ', line, re.IGNORECASE):  # TODO: failed to make a single regex
                    ifpieces = re.findall('if (.*) then (.*) else (.*)', line, re.IGNORECASE)
                    try:
                        results = self._check_condition(variables, ifpieces[0][0])
                    except Exception:
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: {}'.format(line_num + 1, results[1]) }

                    if results:
                        line = ifpieces[0][1]
                    else:
                        line = ifpieces[0][2]
                else:
                    ifpieces = re.findall('if (.*) then (.*)', line, re.IGNORECASE)
                    try:
                        results = self._check_condition(variables, ifpieces[0][0])
                    except Exception:
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: {}'.format(line_num + 1, results[1]) }

                    if results:
                        line = ifpieces[0][1]
                    else:
                        line_num += 1
                        continue

            # COMMAND PARSING
            # splitting multiple commands (&)
            parts = re.split(r'\s*&\s*', line)
            for part in parts:
                command = re.findall(r'^(\S+)', part)[0].lower()
                if command == 'end':
                    # send accumulated message and show accumulated buttons
                    if is_proc:
                        if switch_mode_to_choice:
                            logging.debug('Line %s: Leaving "proc" call, switching to choice mode,')
                            return { 'mode': BookMode.MODE_CHOICE, 'text': proc_message, 'choices': proc_buttons }
                        elif return_message:
                            logging.debug('Line %s: Leaving "proc" call, returning %s.', line_num, proc_message)
                            return { 'mode': BookMode.MODE_TEXT, 'text': proc_message }
                        logging.debug('Line %s: Leaving "proc" call.', line_num)
                        return
                    elif len(self.cur_buttons) > 0:
                        return { 'mode': BookMode.MODE_CHOICE, 'text': self.cur_message, 'choices': self.cur_buttons }
                    else:
                        continue
                elif command == 'goto':
                    # jump to label
                    results = re.findall('^' + command + r'\s(.*)', part, re.IGNORECASE)
                    if len(results) > 0:
                        #label = results[0]
                        label = self._make_string_replacements(variables, results[0])
                        label = self._set_common_label_name(variables, label)  # changes label name for common labels
                        #line_num = self.labels[label]
                        line_num = labels[label]
                        if line_num is None:
                            return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Команда "goto" указывает на несуществующую метку {}!'.format(line_num + 1, results[0]) }
                    else:
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Команда "goto" должна указывать на метку!'.format(line_num + 1) }
                elif command in ('cls', 'anykey', 'pause'):  # list of ignored commands
                    continue
                elif command in ('p', 'pln', 'print', 'println'):  # adding text to section
                    results = re.findall('^' + command + r'\s(.*)', part, re.IGNORECASE)
                    if len(results) > 0:
                        new_text = results[0]
                        new_text = self._make_string_replacements(variables, new_text)
                    else:
                        new_text = ''
                    if command in ('pln', 'println'):
                        new_text += '\n'
                    if return_message:
                        proc_message += new_text
                    else:
                        self.cur_message += new_text
                elif command == 'btn':  # add button
                    results = re.findall('^' + command + r'\s(.*)', part, re.IGNORECASE)
                    if len(results) > 0:
                        params = re.split(r'\s*,\s*', results[0])
                        if len(params) > 1:
                            # add a button
                            #button_label = params[0]
                            #button_text = params[1]
                            button_label = self._make_string_replacements(variables, params[0])
                            button_text = self._make_string_replacements(variables, params[1])
                            if is_proc:
                                proc_buttons.append({'label': button_label, 'desc': button_text})
                                switch_mode_to_choice = True
                            else:
                                self.cur_buttons.append({'label': button_label, 'desc': button_text})
                            logging.debug('Line %s: Added button %s.', line_num, params[0])
                        else:
                            return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: У команды "{}" должно быть не менее 2 параметров (метка и описание)!'.format(line_num + 1, command) }
                    else:  # choice has no parameters!
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: У команды "{}" отсутствуют параметры!'.format(line_num + 1, command) }
                elif '=' in part:  # add variable operation event
                    sides = re.findall(r'(\S+)\s*=\s*(.*)', part)
                    if len(sides) and len(sides[0]) > 1:
                        results = self._assign_variable(variables, sides[0][0], sides[0][1], part.startswith('instr '))
                        if len(results) == 1:
                            logging.debug('Line %s: Variable %s was set to %s.', line_num, sides[0][0], results[0])
                        else:
                            return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: {}'.format(line_num + 1, results[1]) }
                    else:
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Не удалось выпонить операцию присвоения из-за синтаксической ошибки!'.format(line_num + 1) }
                elif command == 'invkill':  # inventory purge
                    results = re.findall('^' + command + r'\s(.*)', part, re.IGNORECASE)
                    if len(results):
                        variables.pop(results[0], None)
                        logging.debug('Line %s: Inventory item purged.', line_num)
                    else:
                        self._variables_purge(variables, True)
                        logging.debug('Line %s: All inventory items purged.', line_num)
                elif command == 'perkill':  # variables purge
                    self._variables_purge(variables, False)
                    logging.debug('Line %s: All variables purged.', line_num)
                elif command in ('inv+', 'inv-'):  # inventory item add/remove
                    results = re.findall('^' + re.escape(command) + r'\s(.*)', part, re.IGNORECASE)
                    if len(results):
                        params = re.split(r'\s*,\s*', results[0])
                        if len(params) > 1:
                            params[0], params[1] = params[1], params[0]  # switch item and amount
                            try:
                                params[1] = float(params[1])
                            except ValueError:
                                return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Не удалось конвертировать "{}" в дробное число.'.format(line_num + 1, params[1]) }
                        else:
                            params.append(float(1))
                        if command == 'inv-':
                            params[1] = -params[1]
                        self._inventory_add(variables, params[0], params[1])
                        logging.debug('Line %s: Added %s pcs of inventory item %s.', line_num, params[1], params[0])
                    else:  # event has no parameters!
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: У команлы "{}" отсутствуют параметры!'.format(line_num + 1, command) }
                elif command == 'proc':  # call label as a procedure (get back after)
                    # save a return point and go to label
                    results = re.findall('^' + command + r'\s(.*)', part, re.IGNORECASE)
                    if len(results):
                        #label = results[0]
                        label = self._make_string_replacements(variables, results[0])
                        label = self._set_common_label_name(variables, label)
                        proc_result = self.interact(lines, labels, variables, mode=BookMode.MODE_PROC, label=label, return_message=return_message)
                        # обработка вариантов возврата
                        if proc_result is not None and proc_result['mode'] == BookMode.MODE_ERROR:
                            return proc_result
                        else:
                            if return_message:
                                proc_message += proc_result['text']
                    else:
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: Команда "proc" должна указывать на метку!'.format(line_num + 1) }
                elif command == 'input':  # string input by user
                    results = re.findall('^' + command + r'\s(.*)', part, re.IGNORECASE)
                    if len(results):
                        return { 'mode': BookMode.MODE_INPUT, 'text': self.cur_message, 'input_var': results[0], 'line_num': line_num + 1 }
                    else:
                        return { 'mode': BookMode.MODE_ERROR, 'text': 'Строка {}: У команды "input" должен быть параметр -- переменная!'.format(line_num + 1) }
                else:
                    logging.debug('Line %s: Unknown command "%s".', line_num, command)
                    #self.no_errors_yet = False
                    #break
            line_num += 1
        return { 'mode': BookMode.MODE_ERROR, 'text': 'Книга закончилась -- игра прекращена.' }

    def open_inventory(self, variables: dict):
        # WORK WITH INVENTORY
        inv_message = 'Содержимое инвентаря:\n'
        inv_buttons = []
        # add inventory actions
        #for label_key, label_value in self.variables.items():
        for label_key in variables.keys():
            if label_key.lower().startswith('use_inv'):
                if label_key.lower() == 'use_inv':
                    inv_buttons.append({'label': label_key, 'desc': 'Осмотреть инвентарь'})
                else:
                    inv_buttons.append({'label': label_key, 'desc': label_key[8:]})
        for var_key, var_value in variables.items():
            try:
                #amount = float(var_value)
                if isinstance(var_value, float):
                    str_value = '{0:g}'.format(var_value)
                else:
                    str_value = var_value
            except ValueError:
                str_value = str(var_value)
                logging.debug('Variable %s value %s is not a float!', var_key, var_value)
            if var_key.startswith('inv_'):  # add inventory items
                #if amount == 1:
                if var_value == 1:
                    inv_buttons.append({'label': '__item_' + var_key[4:], 'desc': var_key[4:]})
                else:
                    #inv_buttons.append({'label': '__item_' + var_key[4:], 'desc': var_value + ' ' + var_key[4:]})
                    inv_buttons.append({'label': '__item_' + var_key[4:], 'desc': str_value + ' ' + var_key[4:]})
            else:  # showing variable values
                pass

        return { 'mode': BookMode.MODE_CHOICE, 'text': inv_message, 'choices': inv_buttons }

    def open_item_actions(self, labels: dict, item: str):
        inv_message = 'Доступные действия с ' + item + ':'
        inv_buttons = []

        for label_key in labels.keys():
            results = re.findall('^use_([^_]*)_?(.*)', label_key, re.IGNORECASE)
            if len(results) and item == results[0][0]:
                if results[0][1] == '':
                    inv_buttons.append({'label': label_key, 'desc': 'Осмотреть'})
                else:
                    inv_buttons.append({'label': label_key, 'desc': results[0][1]})

        return { 'mode': BookMode.MODE_CHOICE, 'text': inv_message, 'choices': inv_buttons }


# Matches '=', but ignores '>=', '<='.
_REGEX_EQUAL = r'(?<=[^!<>=])=(?=[^=])'

# Matches rnd and rndN 
_REGEX_RND = r'(?:\b)(rnd\d*)(?:\b)'

# Matches variable names
_REGEX_VARIABLES = r'(not\s|or\s|and\s)?([A-Za-zА-Яа-я][^=<>!()]*?)(?:\s*(\snot|\sor|\sand|==|<|<=|>|>=|!=|<>|\+|-|\(|\)|$))'

# TODO: What is it?
_REGEX_VARIABLES_ONLY = r'(?:\b)([A-Za-zА-Яа-я]\w*)(?:\b)'

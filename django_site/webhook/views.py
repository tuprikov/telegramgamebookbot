"""
Forwards messages to GamebookMessageProcessor, gets the results and sends them to the Telegram users
"""
import json
import logging

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from helpers.gamebook_message_processor import GamebookMessageProcessor
from services.telegram_bot_service import Keyboard, TelegramBotService

from webhook.models import User

bot = TelegramBotService()
bookshelf = {} # list of active books (cache)
LOG_FORMAT = ''
logging.basicConfig(level=logging.DEBUG)

@csrf_exempt
@require_POST
def get_message(request):
    """
    Entry point for Telegram webhook messages
    """
    jsondata = request.body
    data = json.loads(jsondata)
    if 'message' in data:
        msg = data['message']
        if 'from' in msg and 'chat' in msg and 'text' in msg:
            user_id = msg['from']['id']
            msg_text = msg['text']
            logging.debug('Message from %s (%s): %s', user_id, msg['from']['first_name'], msg_text)
            if msg['from']['is_bot']:
                logging.debug('Message sent by bot, ignored.')
                return
            if msg['chat']['type'] != 'private':
                logging.debug('Message is not private, ignored.') # TODO: switch to private messaging
                return
            chat_id = msg['chat']['id']

            # try to find user, add if not found
            try:
                msg_user = User.objects.get(pk = user_id)
                logging.debug('User %s found.', str(msg_user))
            except User.DoesNotExist:
                msg_user = User.objects.create(pk = user_id, first_name = msg['from']['first_name'], chat_id = chat_id)
                # variables = {}
                # last_interaction = None
                # last_inv_interaction = None
                # last_inv_item_interaction = None
                logging.debug('User %s NOT found, created.', str(msg_user))
            gmp = GamebookMessageProcessor()
            logging.debug('Starting GMP.process with bookshelf %s for user %s with text %s.',
                          bookshelf, msg_user, msg_text)
            reply = gmp.process(bookshelf, msg_user, msg_text)
            for reply_msg in reply:
                if reply_msg.choices is None:
                    kbd = Keyboard(remove_keyboard=True)
                else:
                    kbd = Keyboard(reply_msg.choices, True, True)
                logging.debug('Sending message to %s: %s', chat_id, reply_msg.text)
                message_text = reply_msg.text if reply_msg.text != '' else '(no message was generated)'
                bot.send_message(chat_id, message_text, kbd.__dict__)
        else:
            logging.debug('Unparsed message: %s', msg)
    return HttpResponse()

from django.contrib import admin
from webhook.models import Book, User, UserBookInteraction, UserBookVariable

admin.site.register(Book)
admin.site.register(User)
admin.site.register(UserBookInteraction)
admin.site.register(UserBookVariable)

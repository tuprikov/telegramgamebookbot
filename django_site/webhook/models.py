import os

from django.db import models

from utils.urql_parser import URQLParser

class Book(models.Model):
    name = models.CharField(max_length=200)
    file_name = models.CharField(max_length=1024)
    supports_inventory = models.BooleanField(default=True)

    BOOKS_PATH = os.path.normpath(os.path.join(os.getcwd(), 'books'))
    lines = None # processed (from a file) and cached lines of the book
    labels = None
    #lines = [] # processed (from a file) and cached lines of the book

    #def __init__(self): # TypeError: __init__() takes 1 positional argument but 5 were given
        #super().__init__()
        #self.lines = []
        #self.labels = {}

    def __str__(self):
        return self.name
    def __repr__(self):
        return self.__str__()

    def full_path(self):
        return os.path.normpath(os.path.join(self.BOOKS_PATH, self.file_name))

class UserMode(models.IntegerChoices):
    START = 0           # Welcoming message, immediately set to 'book_choice'
    BOOK_CHOICE = 1     # Waiting for book name. If the book is found, move to 'book_open', else move back to 'start'
    BOOK_OPEN = 2       # Main status. Book processing in progress.
    BOOK_ABORT = 3      # Waiting for user's decition whether to abort or continue the game
    INVENTORY = 4       # Inventory (list of items)
    INVENTORY_ITEM = 5  # Inventory item actions

class InteractionType(models.IntegerChoices):
    GENERAL = 0
    INVENTORY = 1
    INVENTORY_ITEM = 2

class User(models.Model):
    first_name = models.CharField(max_length=100)
    chat_id = models.IntegerField()
    #chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    current_book = models.ForeignKey(Book, on_delete=models.SET_NULL, null=True)
    current_line = models.IntegerField(null=True)
    mode = models.IntegerField(choices=UserMode.choices, default=UserMode.START)
    input_variable = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.first_name
    def __repr__(self):
        return self.__str__()

class UserBookVariable(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=1000, null=True)
    float_value = models.FloatField(null=True)

    def __str__(self):
        if self.float_value is None:
            value_type = 'String'
            the_value = self.value
        else:
            value_type = 'Float'
            the_value = self.float_value
        return '({}) {} = {}'.format(value_type, self.name, the_value)
    def __repr__(self):
        return self.__str__()

class UserBookInteraction(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE)
    type = models.IntegerField(choices=InteractionType.choices, default=InteractionType.GENERAL)
    book_mode = models.IntegerField()
    text = models.TextField()
    choices = models.TextField()

    def __str__(self):
        return 'Type: {}, book mode: {}.'.format(self.type, self.book_mode)
    def __repr__(self):
        return self.__str__()

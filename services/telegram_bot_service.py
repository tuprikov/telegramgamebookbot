import json
import logging
import os
import requests

class TelegramBotService:
    serviceurl = 'https://api.telegram.org/bot'
    url = ''

    def __init__(self):
        bot_token = self._get_bot_token_()
        if bot_token is None:
            logging.critical('No bot token is set!')
            quit()
        self.url = self.serviceurl + bot_token + '/'

    def _get_bot_token_(self):
        return os.environ.get('BOT_API_TOKEN')

    def send_message(self, chat_id, text, reply_markup = None, reply_to_message_id = None):
        if reply_markup is None:
            keyboard_markup = None
        else:
            keyboard_markup = json.dumps(reply_markup)
        params = {'chat_id': chat_id, 'text': text[:4000], 'reply_markup': keyboard_markup, 'reply_to_message_id': reply_to_message_id}
        response = requests.post(self.url + 'sendMessage', data = params)
        if len(text) > 4000:
            self.send_message(chat_id, text[4000:], reply_markup, reply_to_message_id)
        return response

    def set_webhook(self, webhook_url : str):
        public_key_file = {'certificate': open('./gamebook_public.pem')}
        params = {'url': webhook_url}
        response = requests.post(self.url + 'setWebhook', files = public_key_file, data = params)
        return response

    def get_webhook_info(self):
        response = requests.get(self.url + 'getWebhookInfo', )
        return response

class Keyboard:
    keyboard = None
    resize_keyboard = None
    one_time_keyboard = None
    remove_keyboard = None

    def __init__(self, keyboard = None, resize_keyboard = True, one_time_keyboard = True, remove_keyboard = False):
        if remove_keyboard:
            self.remove_keyboard = remove_keyboard
        else:
            self.keyboard = keyboard
            self.resize_keyboard = resize_keyboard
            self.one_time_keyboard = one_time_keyboard

    def __str__(self):
        return self.keyboard
